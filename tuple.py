tuple = (1,2,3,'red','green')
list =[1,2,3,'red','green']
l = [1,29,987,543,234,87,3]
# print(dir(tuple))
# print(dir(list))

# list.reverse()
# l.sort()
# # Difference between delete and clear
# # Difference between list and tuple
# # Difference between remove ,pop and delete
#
# l.clear()
# print(l)
# del l
# print(list)
# print(l)
## TUPLE IS IMMUTABLE
# for i in range(len(tuple)):
#     print(i,': ',tuple[i])
#
tup1 = ((1,2,3),(4,5,6))
tup2 = ((1,2,3),(4,5,6))
tup3 = ((0,0,0),(0,0,0))
# for i in range(len(tup1)):
#     for j in range(len(tup1[i])):
#         tup3[i][j] = tup1[i][j] + tup2[i][j]
# print(tup3)
print(type(tup1))
a = (1,2,3,54,3,6,8,["apple","cherry",(0.1,0.2,0.3),"grapes"],"ab",None,True, {1,2},{"name":"aman"})
print(a[7][2][2])
print(a[12]["name"])
# b = [10,]
# # print(type(c))
# # print(max(a))
# # print(min(a))
# print(a[2:6])
# print(a[-7:])
# for i in range(1,len(a)+1):
#     print(a[-i])
mydict = {"name":"abc",0:"hello","roll_no":234,"cars":("BMW","Toyota")}
mydict["name"]="something"
print(mydict["cars"])
for key in mydict.keys():
    print(key)
for value in mydict.values():
    print(value)
for item in mydict.items():
    print(item)

print(mydict.__doc__)
