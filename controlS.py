# Control Flow Statements
#
# 1. if
# 2. if else
# 3. if elif else
# 4. Nested if
# for i in range(10):
#     if i%2==1:
#         print("inside if statement:",i)
#     # print(i)

# item = input("Enter Element To search")
# list = [1,5,7,3,5,8,0,7,"Aman"]
# for i in range(len(list)):
#     if str(list[i]) == item:
#         print("Element found at location",i)


# a = 10
# if a<=10:
#     print("A is Lesser")
# else:
#     print("A is Greater ")

# student1 = {"name":"abc","age":10}
# student2={"name":"xyz","age":20}
# if student1["age"]>student2["age"]:
#     print("{} is Elder".format(student1["name"]))
# else:
#     print("{} is Elder".format(student2["name"]))

## If elif else
day=input("""***Check Working Day****
**** Type 'quit' to Exit***\n""").title()
while True:
    if day == "Monday":
        print("day is Monday")
        break
    elif day == "Tuesday":
        print("day is Tuesday")
        break
    elif day == "Wednesday":
        print("day is Wednesday")
        break
    elif day == "Thrusday":
        print("day is Thrusday")
        break
    elif day == "Friday":
        print("day is Friday")
        break
    elif day == "Saturday" or day =="Sunday":
        print("{} is not working!!!".format(day))
        break
    elif day == "quit":
        break
    else:
        print("Wrong Input")
        break
