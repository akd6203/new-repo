a = {1,2,3,3,3,4,4,5,5,5,6,7}
print(a)


a = [1,2,3,'a','b','b',1,'c']
myset = set(a)
print(myset)

print('ab' not in a )

#Set Operations
## 1. Add Elements
s =set()
s.add('a')
s.add('python')
print(s)
roll_no = set()
# while True:
#     rno = int(input("Enter Roll Number"))
#     roll_no.add(rno)
#     print(roll_no)
# print(type(s))

## Set UNION
x = {1,2,3,7,9}
y = {1,2,4,5,6}
union = x|y
u2 = x.union(y)
print("Set Union:"+ repr(union))
print("Union Using method 2",u2)

# Set Intersection
intersect = x & y
i2 = x.intersection(y)
print("\n\nintersection :", intersect)
print("Intersection Using method 2",i2)

#Set Difference
# diff = x-y
diff = y-x
print("Set Difference" , diff)
x.clear()
print(x)
